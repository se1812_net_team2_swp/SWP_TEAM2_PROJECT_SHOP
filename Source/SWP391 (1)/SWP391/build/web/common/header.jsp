<%-- 
    Document   : header
    Created on : May 20, 2024, 11:45:12 AM
    Author     : tungn
--%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header>
            <div class="header__area">
                <div class="header__top d-none d-sm-block">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-6 col-lg-6 col-md-5 d-none d-md-block">
                                <div class="header__welcome">
                                    <span>Welcome to TGear</span>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-7">
                                <div class="header__action d-flex justify-content-center justify-content-md-end">
                                    <ul>
<!--                                        <li><a href="#">My Account</a></li>
                                        <li><a href="#">My Wishlist</a></li>-->
                                        <li><a href="login">Sign In</a></li>
                                        <li><a href="register">Sign Up</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header__info">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-4 col-lg-3">
                                <div class="header__info-left d-flex justify-content-center justify-content-sm-between align-items-center">
                                    <div class="logo">
                                        
                                        <a href="home"><img src="${sessionScope.logo.img_path}" style="width: 70%" alt="logo"></a>
                                    </div>
                                    <div class="header__hotline align-items-center d-none d-sm-flex  d-lg-none d-xl-flex">
                                        <div class="header__hotline-icon">
                                            <i class="fal fa-headset"></i>
                                        </div>
                                        <div class="header__hotline-info">
                                            <span>${sessionScope.nameh}</span>
                                            <h6><a href="">${sessionScope.hotline.title}</a></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-8 col-lg-9">
                                <div class="header__info-right">
                                    <div class="header__search f-left d-none d-sm-block">
                                        <form action="#">
                                            <div class="header__search-box">
                                                <input type="text" placeholder="Search For Products...">
                                                <button type="submit">Search</button>
                                            </div>
                                            <div class="header__search-cat">
                                                <select>
                                                    <c:forEach items="${sessionScope.cate}" var="c">
                                                        <option>${c.category_name}</option>
                                                    </c:forEach>

                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="cart__mini-wrapper d-none d-md-flex f-right p-relative">
                                        <a href="javascript:void(0);" class="cart__toggle">
                                            <span class="cart__total-item">01</span>
                                        </a>
                                        <span class="cart__content">
                                            <span class="cart__my">My Cart:</span>
                                            <span class="cart__total-price">$ 255.00</span>
                                        </span>
                                        <div class="cart__mini">
                                          <div class="cart__close"><button type="button" class="cart__close-btn"><i class="fal fa-times"></i></button></div>
                                          <ul>
                                              <li>
                                                <div class="cart__title">
                                                  <h4>My Cart</h4>
                                                  <span>(1 Item in Cart)</span>
                                                </div>
                                              </li>
                                              <li>
                                                <div class="cart__item d-flex justify-content-between align-items-center">
                                                  <div class="cart__inner d-flex">
                                                    <div class="cart__thumb">
                                                      <a href="product-details.jsp">
                                                        <img src="assets/img/shop/product/cart/cart-mini-1.jpg" alt="">
                                                      </a>
                                                    </div>
                                                    <div class="cart__details">
                                                      <h6><a href="product-details.jsp"> Samsung C49J89: £875, Debenhams Plus  </a></h6>
                                                      <div class="cart__price">
                                                        <span>$255.00</span>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div class="cart__del">
                                                      <a href="#"><i class="fal fa-trash-alt"></i></a>
                                                  </div>
                                                </div>
                                              </li>
                                              <li>
                                                <div class="cart__sub d-flex justify-content-between align-items-center">
                                                  <h6>Car Subtotal</h6>
                                                  <span class="cart__sub-total">$255.00</span>
                                                </div>
                                              </li>
                                              <li>
                                                <a href="checkout.jsp" class="t-y-btn w-100 mb-10">Proceed to checkout</a>
                                                <a href="cart.jsp" class="t-y-btn t-y-btn-border w-100 mb-10">view add edit cart</a>
                                              </li>
                                          </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header__bottom">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-6 col-6">
                              <div class="header__bottom-left d-flex d-xl-block align-items-center">
                                <div class="side-menu d-xl-none mr-20">
                                  <button type="button" class="side-menu-btn offcanvas-toggle-btn"><i class="fas fa-bars"></i></button>
                                </div>
                                <div class="main-menu d-none d-md-block">
                                    <nav>
                                        <ul>
                                            <c:forEach items="${sessionScope.menu}" var="m">
                                                <li>
                                                <a href="${m.title.toLowerCase()}">${m.title}</a>
                                               
                                            </li>
                                            </c:forEach>
                                            
                                            
                                                
                                         
<!--                                            <li>
                                                <a href="about.html">Other<i class="far fa-angle-down"></i></a>
                                                <ul class="submenu">
                                                    <li><a href="login">My Account</a></li>
                                                    <li><a href="register">Order History</a></li>
                                                    <li><a href="cart.jsp">Cart</a></li>
                                                    <li><a href="wishlist.jsp">Wishlist</a></li>
                                                    <li><a href="check-out.jsp">Checkout</a></li>
                                                </ul>
                                            </li>-->
                                        </ul>
                                    </nav>
                                </div>
                              </div>
                            </div>
                            <div class="col-xl-3 col-lg-3  col-sm-6  col-6 d-md-none d-lg-block">
<!--                                <div class="header__bottom-right d-flex justify-content-end">
                                    <div class="header__currency">
                                        <select>
                                            <option>USD</option>
                                            <option>VND</option>
                                            <option>EUR</option>
                                        </select>
                                    </div>
                                    <div class="header__lang d-md-none d-lg-block">
                                        <select>
                                            <option>English</option>
                                            <option>Vietnamese</option>
                                        </select>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</header>