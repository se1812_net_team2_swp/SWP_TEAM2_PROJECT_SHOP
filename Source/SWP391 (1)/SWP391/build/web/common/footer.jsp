<%-- 
    Document   : footer
    Created on : May 20, 2024, 12:00:01 PM
    Author     : tungn
--%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<footer>
    <div class="footer__area footer-bg">
        <div class="footer__top pt-80 pb-40">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7 col-lg-12">
                        <div class="footer__top-left">
                            <div class="row">
                                <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6">
                                    <div class="footer__widget">
                                        <div class="footer__widget-title mb-45">
                                            <div class="footer__logo">
                                                <a href="index.jsp"><img src="${sessionScope.logo.img_path}" style="width: 70%" alt=""></a>
                                            </div>
                                        </div>
                                        <div class="footer__widget-content">
                                            <div class="footer__hotline d-flex align-items-center mb-30">
                                                <div class="icon mr-15">
                                                    <i class="fal fa-headset"></i>
                                                </div>
                                                <div class="text">
                                                    <h4>${sessionScope.nameh}:</h4>
                                                    <span><a href="tel:1xxx-xxx-xxx">${sessionScope.hotline.title}</a></span>
                                                </div>
                                            </div>
                                            <div class="footer__info">
                                                <ul>
                                                     <c:forEach items="${sessionScope.liFooterR1}" var="fR1">
                                                         <li><span><a href="#">${fR1.title}</a></span></li>
                                                            </c:forEach>  
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-7 col-lg-6 col-md-6 col-sm-6">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                            <div class="footer__widget">
                                                <div class="footer__widget-title">
                                                    <h4>${sessionScope.about}</h4>
                                                </div>
                                                <div class="footer__widget-content">
                                                    <div class="footer__link">
                                                        <ul>
                                                            <c:forEach items="${sessionScope.liFooterL}" var="fL">
                                                                <li><a href="#">${fL.title}</a></li>
                                                            </c:forEach>
                                                            
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                            <div class="footer__widget">
                                                <div class="footer__widget-title">
                                                    <h4>${sessionScope.cs}</h4>
                                                </div>
                                                <div class="footer__widget-content">
                                                    <div class="footer__link">
                                                        <ul>
                                                            <c:forEach items="${sessionScope.liFooterL1}" var="fL1">
                                                                <li><a href="#">${fL1.title}</a></li>
                                                            </c:forEach>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-12">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="footer__widget">
                                    <div class="footer__widget-title">
                                        <h4>${sessionScope.info}</h4>
                                    </div>
                                    <div class="footer__widget-content">
                                        <div class="footer__link">
                                            <ul>
                                                <c:forEach items="${sessionScope.liFooterL2}" var="fL2">
                                                                <li><a href="#">${fL2.title}</a></li>
                                                            </c:forEach>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <div class="footer__widget">
                                    <div class="footer__widget-title">
                                        <h4>${sessionScope.td}</h4>
                                    </div>
                                    <div class="footer__widget-content">
                                        <div class="footer__link">
                                            <ul>
                                                  <c:forEach items="${sessionScope.liFooterR1}" var="fR1">
                                                                <li><a href="#">${fR1.title}</a></li>
                                                            </c:forEach>                 
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom pt-60 pb-60">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="footer__links text-center">
                            <p>
                                <c:forEach items="${sessionScope.cate}" var="ca">
                                                                <a href="#">${ca.category_name}</a>
                                                            </c:forEach>   
                              
<!--                                <a href="#">Air Conditioners</a>
                                <a href="#">Audios & Theaters</a>
                                <a href="#">Car Electronics</a>
                                <a href="#">Office Electronics</a>
                                <a href="#">TV Televisions</a>
                                <a href="#">Washing Machines</a>-->
                            </p>
                            
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__copyright pt-30 pb-35 footer-bottom-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6">
                        <div class="footer__copyright-text">
                            <p>Copyright © <a href="index.jsp">TGear.</a> All Rights Reserved. <a href="#">Team2_SWP391_K17.</a></p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="footer__payment f-right">
                            <a href="#" class="m-img"><img src="assets/img/icon/payment.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>